# NTNU Beta - Web Page

This is a web application created to present NTNU Betas projects/services. Users associated with NTNU can access
the application by logging in with FEIDE.
A secondary registration option is implemented to allow external users to access this site. It requires an administrator
to approve the request.
As an administrator one can also create, edit and delete projects/services, as well as manage tickers.
Everyone with access to the page can share concerns and issues they may have with features in the projects.
These tickers are available to all users, but can only be resolved by administrators.

## Getting Started

This application runs entirely on Docker. All the application processes are running in cleanly separated docker containers.

### Prerequisites

The only prerequisite for running this is a working docker installation. Both docker and docker-compose are required. Follow the instructions from the official documentation:

1. Docker

- Docker Desktop for Mac: https://docs.docker.com/docker-for-mac/install/
- Docker Desktop for Windows: https://docs.docker.com/docker-for-windows/
- Docker for Linux:

On desktop systems like Docker Desktop for Mac and Windows, Docker Compose is included as part of those desktop installs.

2. Docker Compose

https://docs.docker.com/compose/install/

⚠️ Warning: do not attempt to simply run apt-get install docker docker-compose on older Ubuntu platforms, such as 16.04 (Xenial), as you will get older versions of these utilities.

### Installation

1.  Clone the project

        ``git clone https://gitlab.stud.idi.ntnu.no/simenve/ntnu-beta-web.git``

2.  Navigate to the ntnu-beta-web folder:

        ``cd PATH_TO_PROJECT_FOLDER/ntnu-beta-web/``

3.  Run the following command to build and start the services:

        ``docker-compose build && docker compose up``

4) Visit the application on http://localhost:3000

### Configuration

1. Https credentials

- Set your private key and certificate in `server.js` or preferably add the files to `server/config/`

2. Database configuration
   If you wish to use another database than the one that ships with the application you can change the database credentials:

- Set your db credentials in `server/config/default.json`

3. Json Web Token secret

- Set your jwt secret in `server/config/default.json`

4. Third party SAML identification (Feide)

- We are using the npm package [passport-saml](https://www.npmjs.com/package/passport-saml) to handle XML requests and responses
- Set the metadata for your identity provider in `server/config/default.json`
- Provide your metadata (entityID, SSO and SLO) to your SAML IDP
- Update `SSO_redirect` and `SLO_redirect` in `server/config/default.json` to `https://<DOMAIN>/` and `https://<DOMAIN>:3000/login`

5. Update routes to the backend API calls

- We've used a proxy during development that routes every call from frontend to backend to `localhost:5000/...`
- These backend API calls from the frontend needs to be updated manually before production

6. Create adminuser

- The first adminuser has to be added manually to the database

### Run development servers

1. Run the backend

- `npm run server` from beta-web/server

2. Run the frontend

- `npm start` from beta-web/

### Roadmap
