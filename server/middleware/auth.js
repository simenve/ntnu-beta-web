// Export middleware function to be used by routes to authenticate the user

const jwt = require("jsonwebtoken");
const config = require("config");

module.exports = function(req, res, next) {
  // console.log(req.isAuthenticated());
  // Check if user is logged into Feide
  if (req.isAuthenticated()) {
    // console.log(req.session.passport.user);
    next();

    // Check if user is logged in
  } else {
    // console.log("Not feide user");
    // Get token from header
    const token = req.header("x-auth-token");

    // Check if not token
    if (!token) {
      return res.status(401).json({ msg: "Authorization denied" });
    }

    // Verify token
    try {
      const decoded = jwt.verify(token, config.get("jwtSecret"));

      // Make req.user available to the next() function
      req.user = decoded.user;
      next();
    } catch (err) {
      res.status(401).json({ msg: "Token is not valid" });
    }
  }
};
