const SamlStrategy = require("passport-saml").Strategy;
const config = require("config");

module.exports = function(passport) {
  passport.serializeUser(function(user, done) {
    done(null, user);
  });

  passport.deserializeUser(function(user, done) {
    done(null, user);
  });

  const issuer =
    process.env.NODE_env === "development"
      ? "http://localhost:5000/api/auth/login/callback"
      : config.get("passport.issuer");

  passport.use(
    new SamlStrategy(
      {
        path: config.get("passport.path"),
        entryPoint: config.get("passport.entryPoint"),
        issuer: issuer
        // cert: config.get("passport.cert")
      },
      function(profile, done) {
        return done(null, {
          id: profile.uid,
          email: profile.email
          // displayName: profile.cn,
          // firstName: profile.givenName,
          // lastName: profile.sn
        });
      }
    )
  );
  // passport.use(
  //   new SamlStrategy(
  //     {
  //       path: config.get("passport.path"),
  //       entryPoint: config.get("passport.entryPoint"),
  //       issuer: config.get("passport.issuer")
  //       // cert: config.get("passport.cert")
  //     },
  //     function(profile, done) {
  //       findByEmail(profile.email, function(err, user) {
  //         if (err) {
  //           return done(err);
  //         }
  //         return done(null, user);
  //       });
  //     }
  //   )
  // );
};
