// Export middleware function to be used by routes to authenticate admin user

const user = require('../models/Users');

module.exports = function(req, res, next) {
    // Check if user is admin
    user.isAdmin(req.user.id, function(err, result) {
        if (result !== true) {
            return res.status(401).json({ msg: 'Not authorized '});
        } else {
            next();
        }
    })
}