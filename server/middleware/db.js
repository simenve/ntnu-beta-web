// Setup and export function to connect to MYSQL database

const mysql = require("mysql");
const config = require("config");

const host =
  process.env.NODE_ENV === "development"
    ? config.get("db-remote.host")
    : config.get("db.host");
const user =
  process.env.NODE_ENV === "development"
    ? config.get("db-remote.user")
    : config.get("db.user");
const password =
  process.env.NODE_ENV === "development"
    ? config.get("db-remote.password")
    : config.get("db.password");
const database =
  process.env.NODE_ENV === "development"
    ? config.get("db-remote.database")
    : config.get("db.database");

const connection = mysql.createConnection({
  host: host,
  user: user,
  password: password,
  database: database
});

// connection.connect();

module.exports = connection;
