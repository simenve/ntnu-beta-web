const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");
const passport = require("passport");
const auth = require("../../middleware/auth");
const path = require("path");
const config = require("config");

const SLO_redir =
  process.env.NODE_ENV === "development"
    ? "http://localhost:3000/login"
    : config.get("passport.SLO_redirect");
const SSO_redir =
  process.env.NODE_ENV === "development"
    ? "http://localhost:3000/"
    : config.get("passport.SSO_redirect");

/*
-----
Routes used by Feide IDP
-----
*/

// @route   POST /api/auth/login/callback
// @desc    Callback route for the SAML 2.0 IDP
// @access  Public
router.post(
  "/login/callback",
  express.urlencoded({ extended: false }),
  passport.authenticate("saml", {
    failureRedirect: SLO_redir
    // session: true
    // failureRedirect: config.get("passport.SLO_redirect")
    // failureFlash: true
  }),
  function(req, res) {
    // req.session.save(() => {
    //   res.redirect(SSO_redir);
    // });
    res.redirect(SSO_redir);
  }
);

// @route   GET /api/auth/login
// @desc    Login route, will redirect to the IDP
// @access  Public
router.get(
  "/login",
  passport.authenticate("saml", {
    failureRedirect: "/",
    successRedirect: "/",
    failureFlash: true
  }),
  (req, res) => {
    res.redirect("/");
  }
);

// @route   GET /api/auth/logout
// @desc    Logout route
// @access  Public
router.get("/logout", function(req, res) {
  req.logout();
  res.redirect(SLO_redir);
});

// @route   GET /api/auth/logged_out
// @desc    Logout confirmation. TEST ROUTE
// @access  Public
router.get("/logged_out", function(req, res) {
  res.sendFile(path.resolve("views/logged_out.html"));
});

// @route   GET /api/auth/metadata
// @desc    Endpoint for sending SAML metadata. NOT USED
// @access  Public
router.get("/metadata", (req, res) => {
  // res.sendFile(path.resolve("config/metadata.xml"));
  res.send("Metadata");
});

// @route   GET /api/auth/test/secret
// @desc    Test the auth middleware. TEST ROUTE
// @access  Private
router.get("/test/secret", auth, (req, res) => {
  // res.sendFile(path.resolve("views/secret.html"));
  res.send("Secret page!!!!!");
});

module.exports = router;
