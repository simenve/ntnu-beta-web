const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");
const admin = require("../../middleware/admin");

// @route   GET /api/logged-in
// @desc    Verify user is logged in (check token) and user is admin
// @access  Admin
router.get("/admin", [auth, admin], (req, res) => {
  res.send(true);
});

// @route   GET /api/logged-in
// @desc    Verify user is logged in (check token)
// @access  Private
router.get("/", auth, (req, res) => {
  console.log(req.user);
  res.send(req.user);
});

module.exports = router;
