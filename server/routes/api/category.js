const express = require('express');
const router = express.Router();
const category = require('../../models/Category');
const auth = require('../../middleware/auth');

// @route   POST /api/category/
// @desc    Create a new category
// @access  Private
router.post('/', auth, async (req, res) => {
    try {

        // Create new service using methods from models/Service.js (use async/await)
        const newCategory = {
            label: req.body.label
        }

        //Find the service from the database using methods from models/Service.js (use async/await)
        var labelExists = "";
        await category.getOne(req.body.label, function(err, existingLabel) {
          labelExists = existingLabel;
        });

        // Insert the ticker using methods from models/Category.js (use async/await)
        await category.create(newCategory, (err, category) => {
            // Validate that the category doesn't already exist
            if(labelExists.toString() !== ""){
              console.error("The specified category already exists");
              res.status(400).json("Not allowed");
              return;
            }

            // Respond with OK message
            res.json({ msg: 'New category created!' });
        });

    } catch (err) {
        console.error(err.message);
        res.status(500).json('Server error');
    }
});

// @route   GET /api/category/:category_id
// @desc    Get a category by label
// @access  Public
router.get('/:category_id', async (req, res) => {
    {
      try {
        // Fetch all categories from the database using methods from models/Service.js (use async/await)
        await service.getOne(reg.params.category_id, function(err, category) {
          // Respond with services as json
          res.json(category);
        });
      } catch (err) {
        console.error(err.message);
        res.status(500).json('Server error');
      }
    }
 });

// @route   GET /api/category/
// @desc    Get all categories
// @access  Public
router.get('/', async (req, res) => {
      try {
        // Fetch all categories from the database using methods from models/Service.js (use async/await)
        await category.getAll(function(err, categories) {
          // Respond with services as json
          res.json(categories);
        });
      } catch (err) {
        console.error(err.message);
        res.status(500).json('Server error');
      }
    });

module.exports = router;