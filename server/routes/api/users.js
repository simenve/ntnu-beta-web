// Handle all request for retrieving, creating and updating users

const express = require('express');
const router = express.Router();
const user = require('../../models/Users');
const { check, validationResult } = require('express-validator');
const bcrypt = require('bcryptjs');
const auth = require('../../middleware/auth');
const admin = require('../../middleware/admin');

// @route   GET /api/users/
// @desc    Get all users
// @access  Private
router.get('/', async (req, res) => {
  try {
    // Fetch all users from the database using methods from models/Users.js (use async/await)
    await user.getAll(function(err, users) {

      // Respond with users as json
      if(err) {
        res.status(500).json('Database error: ' + err.message);
      } else {
        res.json(users)
      }
    });
    
  } catch (err) {
    console.error(err.message);
    res.status(500).json('Server error');
  }
});

// @route   POST /api/users/
// @desc    Create new user that will be saved to the database
// @access  Public
router.post('/',
  /*[
    check("email", 'Email is required')
      .not()
      .isEmpty(),
    check("password", 'Password is required')
      .not()
      .isEmpty(),
  ],*/
  async (req, res) => {
    // Check input using express-validator
    // const errors = validationResult(req);
    // if (!errors.isEmpty()) {
    //   return res.status(400).json({ errors: errors.array() });
    // }

    try {

      // Create new user using methods from models/Users.js (use async/await)
      const newUser = {
        email: req.body.email,
        password: req.body.password,
      };

      const salt = await bcrypt.genSalt(10);
      newUser.password = await bcrypt.hash(newUser.password, salt);

      // Save user to DB
      await user.addUser(newUser, (err, service) => {

        if(err) {
          return res.status(500).json('Database error: ' + err.message);
        } else {
          // Respond with OK message
          res.json({ msg: "User added!" });
        }
      });

    } catch (err) {
      console.error(err.message);
      res.status(500).json('Server error');
    }
  }
);

// @route   PUT /api/users/:email
// @desc    Update existing user with email (URL parameter)
// @access  Admin
router.put('/:email', [auth, admin], async (req, res) => {

  try {

      // Find the user from the database using methods from models/Users.js (use async/await)
      var userExists = "";
      await user.getOne(req.params.email,function(err,newUser) {
        userExists = newUser;
      });

      // Get email and other class-fields from URL and body respectively
      const email = req.params.email;
      console.log(email)
      if(req.body.admin && req.body.admin == 'True') {
        var admin  = '1'
      } else if (req.body.admin) {
        var admin = '0'
      }
      if(req.body.access && req.body.access == 'True') {
        var access  = '1'
      } else if (req.body.access) {
        var access = '0'
      }
      const newValues = {
          admin: admin,
          access: access
      }

      // Remove fields where we do not wish to update the values
      for(var key in newValues){
        if(newValues.hasOwnProperty(key)){
          if(newValues[key] == null || newValues[key]=="") delete newValues[key];
        }
      }

      // Edit the user from the database using methods from models/Users.js (use async/await)
      await user.update(newValues,email,(err,user) => {

        // Check that this service exists
        if(userExists.toString()==""){
            console.error("The specified service does not exist");
            res.status(404).json("Not found");
            return;

        } else if (err) {
          console.error("Database error");
          res.status(500).json('Database error: ' + err.message);
          
        } else {
          res.json({msg: "User updated!"})
        }          
      });

  } catch (err) {
      console.error(err.message);
      res.status(500).json('Server error');
  }
});

// @route   DELETE /api/users/:email
// @desc    Delete existing user with email (URL parameter)
// @access  Admin
router.delete('/:email', [auth, admin], async (req, res) => {
    
  try {      
        //Find the user from the database using methods from models/Users.js (use async/await)
        var userExists = "";
        await user.getOne(req.params.email,function(err,newUser) {
          if(err) {
            res.status(500).json('Database error: ' + err.message);
          } else {
            userExists = newUser;
          }
        });

        // Delete the user using methods from models/Users.js (use async/await)
        await user.delete(req.params.email,(err,service) => {

          // Check that this user exists
          if(userExists.toString()==""){
              console.error("The specified user does not exist");
              res.status(404).json("Not found");
              return;

          } else if (err) {
            console.error("The specified user does not exist");
            res.status(500).json('Database error: ' + err.message);

          } else {
            res.json({msg: "User removed!"})
          }   
        });

    } catch (err) {
        console.error(err.message);
        res.status(500).json('Server error');
    }
});

module.exports = router;
