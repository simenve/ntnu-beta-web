const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");
const User = require("../../models/Users");
const jwt = require("jsonwebtoken");
const config = require("config");
const { check, validationResult } = require("express-validator");
const bcrypt = require("bcryptjs");

// @route   POST api/login
// @desc    Auth user and get token
// @access  Public
router.post(
  "/",
  [
    check("email", "Include a valid email").isEmail(),
    check("password", "Password is required").exists()
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const { email, password } = req.body;

    try {
      await User.getOne(email, function(err, user) {
        // See if user exists
        if (!user) {
          return res.status(400).json({ msg: "Invalid credentials" });
        }
        if (user[0].access === 0) {
          return res.status(401).json({ msg: "Need approval from admin" });
        }

        // Validate email & password
        async function checkUser(user, password) {
          const match = await bcrypt.compare(password, user.password);
          if (!match) {
            return res.status(400).json({ msg: "Invalid credentials" });
          }

          const admin = user.admin === 1 ? true : false;

          const payload = {
            user: {
              id: user.email
            }
          };

          jwt.sign(
            payload,
            config.get("jwtSecret"),
            { expiresIn: 14400 }, // 14400 seconds = 4 hours
            (err, token) => {
              if (err) throw err;
              console.log("admin");
              console.log(admin);
              res.json({ token: token, admin: admin });
            }
          );
        }
        checkUser(user[0], password);
      });
    } catch (err) {
      console.error(err);
      res.status(500).send("Server error");
    }
  }
);

module.exports = router;
