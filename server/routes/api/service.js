// Handle all request for retrieving, creating and updating services/projects

const express = require('express');
const router = express.Router();
const service = require('../../models/Service');
const { check, validationResult } = require('express-validator');
const auth = require('../../middleware/auth');
const admin = require('../../middleware/admin');

// @route   GET /api/service/:service_id
// @desc    Get one service with service_id as param. If param === 'categories', will return all categories instead
// @access  Public
router.get('/:service_id', async (req, res) => {
    try {
        // Find the service from the database using methods from models/Service.js (use async/await)
        await service.getOne(req.params.service_id, function(err, service) {
            // Check that this service exists
            if(service.toString() == ""){
              console.error("The specified service does not exist");
              res.status(404).json("Not found");
              return;
            }
            // Respond with services as json
            res.json(service);
        });

    } catch (err) {
        console.error(err.message);
        res.status(500).json('Server error');
    }
  });

// @route   GET /api/service
// @desc    Get all services
// @access  Public
router.get('/', async (req, res) => {
  try {
    // Fetch all services from the database using methods from models/Service.js (use async/await)
    await service.getAll(function(err, services) {
      // Respond with services as json
      res.json(services);
    });
  } catch (err) {
    console.error(err.message);
    res.status(500).json('Server error');
  }
});

// @route   POST /api/service
// @desc    Create new service that will be saved to the database
// @access  Admin
router.post(
  '/',
  [auth, admin,
  [
    check('author', 'Author is required')
      .not()
      .isEmpty(),
    check('title', 'Title is required')
      .not()
      .isEmpty(),
    check('status', 'Status is required')
      .not()
      .isEmpty()
  ]],
  async (req, res) => {

    // Check input using express-validator
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const newService = {
      author: req.body.author,
      title: req.body.title,
      content: req.body.content,
      logo: req.body.logo,
      status: req.body.status,
      extURL: req.body.extURL,
      intURL: req.body.intURL,
      category: req.body.category
    };

    try {

      // Create new service using methods from models/Service.js (use async/await)
      await service.create(newService, (err, service) => {
        // Respond with OK message
        return res.json({ msg: 'New service created!' });
      });

    } catch (err) {
      console.error(err.message);
      return res.status(500).json('Server error');
    }
  }
);

// @route   PUT /api/service/:service_id
// @desc    Update existing service with service_id (URL parameter)
// @access  Admin
router.put('/:service_id', [auth, admin], async (req, res) => {
    try {
        // Find the service from the database using methods from models/Service.js (use async/await)
        var serviceExists = "";
        await service.getOne(req.params.service_id,function(err,newService) {
          serviceExists = newService;
        });

        // Get ID and other class-fields from URL and body respectively
        const id = req.params.service_id;
        const newValues = {
            author:req.body.author,
            title: req.body.title,
            content: req.body.content,
            logo: req.body.logo,
            status: req.body.status,
            extURL: req.body.extURL,
            intURL: req.body.intURL,
            category: req.body.category
        }

        // Remove fields where we do not wish to update the values
        for(var key in newValues){
          if(newValues.hasOwnProperty(key)){
            if(newValues[key] == null || newValues[key]=="") delete newValues[key];
          }
        }

        // TODO Front-end: all "old" values need to be put in the body of this call. (prepopulate)
        // ALSO: Backend: only update non-empty values from front-end (check input)

        // Edit the service from the database using methods from models/Service.js (use async/await)
        await service.update(newValues,id,(err,service) =>{
          // Check that this service exists
          if(serviceExists.toString()==""){
              console.error("The specified service does not exist");
              res.status(404).json("Not found");
              return;
          }

          // Check that the service belongs to this user

          // Respond with OK message
          res.json({msg: "Service updated!"})
        });
        
    } catch (err) {
        console.error(err.message);
        res.status(500).json('Server error');
    }
});

// @route   DELETE /api/service/:service_id
// @desc    Delete existing service with service_id (URL parameter)
// @access  Admin
router.delete('/:service_id', [auth, admin], async (req, res) => {
    try {
        //Find the service from the database using methods from models/Service.js (use async/await)
        var serviceExists = "";
        await service.getOne(req.params.service_id,function(err,newService) {
          serviceExists = newService;
        });

        // Delete the service using methods from models/Service.js (use async/await)
        await service.delete(req.params.service_id,(err,service) =>{
          // Check that this service exists
          if(serviceExists.toString()==""){
              console.error("The specified service does not exist");
              res.status(404).json("Not found");
              return;
          }

          // Check that the service belongs to this user

          // Respond with OK message
          res.json({msg: "Service deleted!"})
        });
    } catch (err) {
        console.error(err.message);
        res.status(500).json('Server error');
    }
});

module.exports = router;
