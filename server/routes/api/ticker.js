// Handle all request for retrieving, creating and updating tickers

const express = require('express');
const router = express.Router();
const ticker = require('../../models/Ticker');
const service = require('../../models/Service');
const auth = require('../../middleware/auth');
const admin = require('../../middleware/admin');

// @route   GET /api/ticker/all/:service_id
// @desc    Get all tickers for a certain service with service_id (URL parameter)
// @access  Public
router.get('/all/:service_id', async (req, res) =>{
    try {

        //Find the service from the database using methods from models/Service.js (use async/await)
        //Check that this service exists
        let serviceExists = "";

        await service.getOne(req.params.service_id,function(err,newService) {
          serviceExists = newService;
        });

        //Find the values from the database using methods from models/Ticker.js (use async/await)
        await ticker.getAll(req.params.service_id,function(err, getTicker) {
          if (serviceExists.toString() == "") {
              console.error("The specified service does not exist");
              res.status(404).json("Not found");
              return;
          }
            res.json(getTicker);
        });

    } catch (err) {
        console.error(err.message);
        res.status(500).json('Server error');
    }
});

// @route   GET /api/ticker/:ticker_id
// @desc    Get one ticker with ticker_id (URL parameter)
// @access  Public
router.get('/:ticker_id', async (req, res) =>{
    try {

        //Find the service from the database using methods from models/Ticker.js (use async/await)
        await ticker.getOne(req.params.ticker_id,function(err, getTicker) {

            //Check that this ticker exists, respond with appropriate error if not
            if(getTicker.toString() == ""){
              console.error("The specified ticker does not exist");
              res.status(404).json("Not found");
              return;
            }

            //respond with ticker as json
            res.json(getTicker);
        });
    } catch (err) {
        console.error(err.message);
        res.status(500).json('Server error');
    }
});

// @route   POST /api/ticker/:service_id
// @desc    Create a new ticker for a certain service with service_id
// @access  Private
 router.post('/:service_id', auth, async (req, res) => {
    try {
        // Auth user

        // Check input using express-validator

        // Create new service using methods from models/Service.js (use async/await)
        const newTicker = {
            title: req.body.title,
            text: req.body.text,
            mail: req.body.mail,
            sid: req.params.service_id
        }

        //Find the service from the database using methods from models/Service.js (use async/await)
        var serviceExists = "";
        await service.getOne(req.params.service_id,function(err,newService) {
          serviceExists = newService;
        });

        // Insert the ticker using methods from models/Ticker.js (use async/await)
        await ticker.create(newTicker, (err, ticker) => {
            // Validate that the service with id='sid' exists
            if(serviceExists.toString() == ""){
              console.error("The specified service does not exist");
              res.status(404).json("Not found");
              return;
            }

            // Respond with OK message
            res.json({ msg: 'New ticker created!' });
        });

    } catch (err) {
        console.error(err.message);
        res.status(500).json('Server error');
    }
});

// @route   DELETE /api/ticker/:ticker_id
// @desc    Delete a ticker with ticker_id (URL parameter)
// @access  Admin
router.delete('/:ticker_id', [auth, admin], async (req, res) => {
    try {
        //Find the ticker from the database using methods from models/Ticker.js (use async/await)
        var tickerExists = "";
        await ticker.getOne(req.params.ticker_id,function(err,newTicker) {
          tickerExists = newTicker;
        });

        // Delete the ticker using methods from models/Ticker.js (use async/await)
        await ticker.delete(req.params.ticker_id,(err,ticker) => {
            // Check that this ticker exists
            if(tickerExists.toString()==""){
                console.error("The specified ticker does not exist");
                res.status(404).json("Not found");
                return;
            }

            // Respond with OK message
            res.json({msg: "Service updated!"})
        });

        } catch (err) {
            console.error(err.message);
            res.status(500).json('Server error');
        }
});

// @route   PUT /api/ticker/:ticker_id
// @desc    Mark a ticker with ticker_id as resolved(URL parameter)
// @access  Public
router.put('/:ticker_id', [auth, admin], async (req, res) => {
    try {
        

        //Find the ticker from the database using methods from models/Ticker.js (use async/await)
        var tickerExists = "";
        await ticker.getOne(req.params.ticker_id,function(err,newTicker) {
          tickerExists = newTicker;
        });

        // Delete the ticker using methods from models/Ticker.js (use async/await)
        await ticker.resolved(req.params.ticker_id,(err,ticker) => {
            // Check that this ticker exists
            if(tickerExists.toString()==""){
                console.error("The specified ticker does not exist");
                res.status(404).json("Not found");
                return;
            }

            // Respond with OK message
            res.json({msg: "Service updated!"})
        });

    } catch (err) {
        console.error(err.message);
        res.status(500).json('Server error');
    }
});

module.exports = router;
