// Define query methods for interacting with the database

var connection = require('../middleware/db.js');

// Object constructor
const Service = function(service) {
    this.author = service.author;
    this.title = service.title;
    this.content = service.content;
    this.logo = service.logo;
    this.status = service.status;
    this.extURL = service.extURL;
    this.intURL = service.intURL;
    this.category = service.category;
}

// @name    Service.create
// @desc    Insert new service into the database
// @input   var newService = {author: 'author', title: 'title', content: 'content', logo: 'logo', status: 'status', extURL: 'extURL', intURL: 'intURL', category: 'category'}
Service.create = function(newService, done) {
        connection.query("INSERT INTO Service set ?", newService, function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    done(err, null);
                }
                else{
                    done(null, res);
                }
            });
};

// @name    Service.getAll
// @desc    Select and return all services from the database
Service.getAll = function(done) {
    connection.query("SELECT * FROM Service", function(err, res) {

        if (err) {
            console.log("error: ", err);
            done(err, null);
        } else {
            done(null, res);
        }
    });
}

// @name    Service.getOne
// @desc    Select one service by id
Service.getOne = function(id,done){
    connection.query("SELECT * FROM Service WHERE id="+connection.escape(id),function(err,res){

      if(err){
        console.log("error: ",err);
        done(err,null);
      }else{
        done(null,res);
      }
    });
}

// @name    Service.update
// @desc    Update a service by id
Service.update = function(updatedValues,id,done){
    connection.query("UPDATE Service SET "+connection.escape(updatedValues)+" WHERE id="+connection.escape(id),function(err,res){

      if(err){
        console.log("error: ",err);
        done(err,null);
      }else{
        done(null,res);
      }
    });
}

// @name    Service.delete
// @desc    Delete a service from the database by id
Service.delete = function(id,done){
  connection.query("DELETE FROM Service WHERE id="+connection.escape(id),function(err,res){

    if(err){
      console.log("error: ",err);
      done(err,null);
    }else{
      done(null,res);
    }
  });
}

module.exports = Service;
