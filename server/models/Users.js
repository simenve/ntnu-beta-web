// Define query methods for interacting with the database

const connection = require('../middleware/db.js');

// Object constructor
const User = function(user) {
    this.email = user.email;
    this.password = user.password;
    this.admin = user.admin;
}


// @name    User.getOne
// @desc    Select and return a specific user together with admin information from the database
User.getOne = async function(email, done) {
    connection.query("SELECT email, admin, access, password FROM Users WHERE email="+connection.escape(email), function(err, res) {

        if (err) {
            console.log("error: ", err);
            done(err, null);
        } else {
            done(null, res);
        }
    });
}

// @name    User.getAll
// @desc    Select and return all users together with admin information from the database
User.getAll = function(done) {
    connection.query("SELECT email, admin, access FROM Users", function(err, res) {

        if (err) {
            console.log("error: ", err);
            done(err, null);
        } else {
            done(null, res);
        }
    });
}

// @name    User.addUser
// @desc    Insert new user into the database
// @input   var newUser = {email: 'email', password: 'password'}
User.addUser = function(newUser, done) {
    connection.query("INSERT INTO Users(email, password) VALUES ("+connection.escape(newUser.email)
    +", "+ connection.escape(newUser.password)+")", function (err, res) {

            if(err) {
                console.log("error: ", err);
                done(err, null);
            }
            else{
                done(null, res);
            }
        });
};

// @name    Service.update
// @desc    Update a service by id
User.update = function(updatedValues,email,done){
  connection.query("UPDATE Users SET "+connection.escape(updatedValues)+" WHERE email="+connection.escape(email),function(err,res){

    if(err){
      console.log("error: ",err);
      done(err,null);
    }else{
      done(null,res);
    }
  });
}

// @name    User.delete
// @desc    Delete a user from the database by email
User.delete = function(email,done){
  connection.query("DELETE FROM Users WHERE email="+connection.escape(email),function(err,res){

    if(err){
      console.log("error: ",err);
      done(err,null);
    }else{
      done(null,res);
    }
  });
}

// @name    User.isAdmin
// @desc    Return true if user is admin
User.isAdmin = function(email, done) {
  connection.query("SELECT admin FROM Users WHERE email="+connection.escape(email), function(err, res) {

    if (err) {
      console.error(err);
      done(err,null);
    } else {
      response = res[0]["admin"] === 1 ? true : false;
      done(null,response);
    }

  });
}

module.exports = User;
