// Define query methods for interacting with the database

var connection = require('../middleware/db.js');

// Object constructor
const Category = function(category) {
  this.label = category.label;
}

// @name    Category.create
// @desc    Insert new category into the database
// @input   var newCategory = {label: 'label'}
Category.create = function(newCategory, done) {
        connection.query("INSERT INTO Category set ?", newCategory, function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    done(err, null);
                }
                else{
                    done(null, res);
                }
            });
};

// @name Category.get
// @desc Select category by label
Category.getOne = function(label, done) {
      connection.query("SELECT * FROM Category WHERE label="+connection.escape(label),function(err,res) {

                if(err) {
                    console.log("error: ", err);
                    done(err,null);
                }
                else{
                    done(null,res);
                }
      });
};

// @name    Service.getAllCategories
// @desc    Select and return all categories from the database
Category.getAll = function(done) {
    connection.query("SELECT * FROM Category", function(err, res) {
        if (err) {
            console.log("error: ", err);
            done(err, null);
        } else {
            done(null, res);
        }
    });
}

module.exports = Category;
