// Define query methods for interacting with the database

var connection = require('../middleware/db.js');

// Object constructor
const Ticker = function(ticker) {
    this.title = ticker.title;
    this.text = ticker.text;
    this.mail = ticker.mail;
    this.sid = ticker.sid;
}

// @name    Ticker.create
// @desc    Insert new ticker into the database
// @input   var newTicket = {title: 'title', text: 'text', ...}
Ticker.create = function(newTicket, done) {
        connection.query("INSERT INTO Ticker set ?", newTicket, function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    done(err, null);
                }
                else{
                    done(null, res);
                }
            });
};

// @name Ticker.getOne
// @desc Select one ticker by id
Ticker.getOne = function(id, done) {
      connection.query("SELECT * FROM Ticker WHERE id="+connection.escape(id),function(err,res) {

                if(err) {
                    console.log("error: ",err);
                    done(err,null);
                }
                else{
                    done(null,res);
                }
      });
};

// @name Ticker.getAll
// @desc Select all tickers from this Service by service_id (sid)
Ticker.getAll = function(sid,done) {
      connection.query("SELECT * FROM Ticker WHERE resolved=0 AND sid="+connection.escape(sid),function(err,res) {

                if(err) {
                    console.log("error: ",err);
                    done(err,null);
                }
                else{
                    done(null,res);
                }
      });
};

// @name Ticker.resolved
// @desc Mark a ticker as resolved by ticker_id
Ticker.resolved = function(id,done){
    connection.query("UPDATE Ticker SET resolved=1 WHERE id="+connection.escape(id),function(err,res){

      if(err){
        console.log("error: ",err);
        done(err,null);
      }else{
        done(null,res);
      }
    });
}

module.exports = Ticker;
