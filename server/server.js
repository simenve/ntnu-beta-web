const express = require("express");
const session = require("express-session");
const app = express();
const cookieParser = require("cookie-parser");
const fs = require("fs");
const https = require("https");
const passport = require("passport");

const config = require("config");
const PORT = process.env.PORT || 5000;

const db = require("./middleware/db");

try {
  db.connect();
  console.log("MySQL DB connected...");
} catch (err) {
  console.log(err.message);
}

// const privateKey = fs.readFileSync(__dirname + "/config/server.key", "utf-8");
// const certificate = fs.readFileSync(__dirname + "/config/server.cert", "utf-8");
const privateKey = fs.readFileSync(
  __dirname + "/config/beta-katalog.it.ntnu.no.key-2019",
  "utf-8"
);
const certificate = fs.readFileSync(
  __dirname + "/config/beta-katalog.it.ntnu.no.crt-2019",
  "utf-8"
);

const credentials = {
  key: privateKey,
  cert: certificate,
  passphrase: config.get("app.sslSecret"),
  requestCert: false,
  // rejectUnauthorized should be true (server certificate will be verified against lists of CAs)
  rejectUnauthorized: false
};

require("./middleware/passport")(passport);

// Init Middleware
// Express comes with npm body-parser, which is used to parse http post requests
// Parse incomming requests with req.body
// app.use(express.json({ extended: false }));

// app.use(cookieParser(config.get("app.session")));
app.use(cookieParser());

app.use(express.json());

app.use(function(req, res, next) {
  // Match the domain you will make the request from
  // res.header("Access-Control-Allow-Origin", "*");
  // res.header("Access-Control-Allow-Origin", [
  //   "http://localhost:3000",
  //   "https://beta-katalog.it.ntnu.no",
  //   "https://beta.it.ntnu.no"
  // ]);

  // Will this work without reverse proxy?
  const corsWhitelist = [
    "http://localhost:3000",
    "https://beta-katalog.it.ntnu.no",
    "https://beta.it.ntnu.no"
  ];
  if (corsWhitelist.indexOf(req.headers.origin) !== -1) {
    res.header("Access-Control-Allow-Origin", req.headers.origin);
  }

  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, x-auth-token, Content-Type, Accept"
  );
  res.header("Access-Control-Allow-Credentials", true);
  res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE");
  next();
});

app.use(
  session({
    resave: true,
    saveUninitialized: true,
    secret: config.get("app.session")
  })
);

// Init passport
app.use(passport.initialize());
app.use(passport.session());

// Define Routes
app.use("/api/auth", require("./routes/api/auth"));
app.use("/api/profile", require("./routes/api/profile"));
app.use("/api/service", require("./routes/api/service"));
app.use("/api/category", require("./routes/api/category"));
app.use("/api/ticker", require("./routes/api/ticker"));
app.use("/api/users", require("./routes/api/users"));
app.use("/api/login", require("./routes/api/login"));
app.use("/api/logged-in", require("./routes/api/logged-in"));

// Test route
app.get("/", (req, res) => {
  res.sendFile(__dirname + "/views/index.html");
});

// Serve static assets in production
if (process.env.NODE_ENV === "production") {
  console.log("env == production");
  // Set static folder
  app.use(express.static("client/build"));

  app.get("*", (req, res) => {
    res.sendFile(path.resolve(__dirname, "client", "build", "index.html"));
  });
}

// Init server
if (process.env.NODE_ENV === "development") {
  app.listen(PORT, () => console.log(`HTTP server running on port ${PORT}`));
} else {
  https
    .createServer(credentials, app)
    .listen(PORT, () =>
      console.log(`HTTPS server running on PORT ${PORT} ...`)
    );
}
