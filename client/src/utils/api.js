const setEndpoint = () => {
  const PORT = process.env.PORT || 5000;
  if (!process.env.NODE_ENV || process.env.NODE_ENV === "development") {
    return `http://localhost:${PORT}/`;
  } else {
    return `https://beta-katalog.it.ntnu.no:${PORT}/`;
  }
};

export const ENDPOINT = setEndpoint();
