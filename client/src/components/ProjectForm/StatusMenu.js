import React from 'react';
import Select from 'react-select';

// Dropdown menu for category dropdown in form
function StatusMenu(props) {
  let status = props.status

  if(status === 'open'){
    status = {label: 'I drift', value: status};
  }
  else if (status === 'testing') {
    status = {label: 'Under uttesting', value: status};
  }
  else if (status === 'closed'){
    status = {label: 'Avsluttet', value: status};
  }

  // Default options for card status
  const statusOptions = [
    { value: 'open', label: 'I drift' },
    { value: 'closed', label: 'Avsluttet' },
    { value: 'testing', label: 'Under uttesting' }
  ];

  return (
    <div className={props.className}>
      <label>Status: *</label>
      <Select
        value={status}
        onChange={props.handleStatus} // Callback method from Form
        options={statusOptions}
        form="form"
      />
    </div>
  );
}


export default StatusMenu;
