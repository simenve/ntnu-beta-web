import React, { Component } from "react";
import axios from "axios";
import { ENDPOINT } from "../../utils/api";

import StatusMenu from "./StatusMenu";
import "./Form.css";

import CreateableSelect from "react-select/creatable";
import Quill from "../Quill/Quill";
import ReactModal from "react-modal";

ReactModal.setAppElement("#root");
class Form extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: null,
      extURL: null,
      content: null,
      author: null,
      logo: null,
      status: null,
      isLoading: false,
      categoryList: [],
      category: undefined,
      showModal: false,
      invalid: true
    };
    this.handleCloseModal = this.handleCloseModal.bind(this);
  }

  // Gets the categories from the database
  componentWillReceiveProps(props) {
    this.setState({ showModal: props.modal });
    this.fetchCategories().then(result => {
      result.sort((a, b) =>
        a.label > b.label ? 1 : b.label > a.label ? -1 : 0
      );
      this.setState({ categoryList: result });
    });
  }

  // Gets all categories from the database
  fetchCategories = async () => {
    let categoryList = [];
    await axios({
      url: ENDPOINT + "api/category",
      method: "GET"
    }).then(res => {
      res.data.forEach(function(category) {
        categoryList.push(category);
      });
    });
    return categoryList;
  };

  // Sets the newly selected category
  handleChangeCategory = (newValue, actionMeta) => {
    this.setState({ category: newValue });
    this.validateInput("category", newValue);
  };

  // Passed as props to Quill component to recieve text from text editor
  getQuill = info => {
    this.setState({ content: info });
    this.validateInput("content", info);
  };

  // Handles actions when form is submited. POST Request and redirection
  handleSubmit = async e => {
    e.preventDefault();
    await axios({
      url: ENDPOINT + "api/service",
      method: "POST",
      headers: { "x-auth-token": this.props.token },
      data: {
        author: this.state.author,
        title: this.state.title,
        content: this.state.content,
        logo: this.state.logo,
        status: this.state.status.value,
        extURL: this.state.extURL,
        category: this.state.category.label
      }
    }).then(res => {
      console.log(res.data);
    });
    this.handleCloseModal();
    this.props.close();
  };

  // Set state according to input
  handleChange = e => {
    e.preventDefault();
    const { name, value } = e.target;
    this.setState({ [name]: value });
    this.validateInput(name, value);
  };

  // Validates that the required fields have been filled in
  validateInput(name, value) {
    /*Maps the fields to assert their current contents.
    This is necessary because the setState function is asynchronous*/
    let title = name === "title" ? value : this.state.title;
    let content = name === "content" ? value : this.state.content;
    let author = name === "author" ? value : this.state.author;
    let category = name === "category" ? value : this.state.category;
    let status = name === "status" ? value : this.state.status;

    // Checks for invalid or empty fields
    if (
      !title ||
      !author ||
      content === "<p><br></p>" ||
      !content ||
      !category ||
      !status
    ) {
      this.setState({ invalid: true });
      return;
    }
    this.setState({ invalid: false });
  }

  // Set state according to status dropdown fields
  handleStatus = Status => {
    this.setState({ status: Status });
    this.validateInput("status", Status);
  };

  // Adds new category to list in state
  handleCreate = inputValue => {
    this.setState({ isLoading: true });
    const newCategory = { label: inputValue, value: inputValue };
    this.validateInput("category", newCategory);
    setTimeout(() => {
      const { categoryList } = this.state;
      this.setState({
        isLoading: false,
        categoryList: [...categoryList, newCategory],
        category: newCategory
      });
      this.handleNewCategory();
    }, 1000);
  };

  // Sends the newly created category to the database
  handleNewCategory = async e => {
    await axios({
      url: ENDPOINT + "api/category/",
      method: "POST",
      headers: { "x-auth-token": this.props.token },
      data: {
        label: this.state.category.label
      }
    }).then(res => {
      console.log(res.data);
    });
  };

  // Closes the form
  handleCloseModal() {
    this.setState({ invalid: true });
    this.setState({ showModal: false });
    this.props.close();
  }

  render() {
    const { isLoading, category } = this.state;

    return (
      <ReactModal
        isOpen={this.state.showModal}
        contentLabel="onRequestClose Example"
        onRequestClose={this.handleCloseModal}
        className="Modal"
        overlayClassName="Overlay"
      >
        <div className="form-wrapper">
          <h1 className="form-title">Opprett tjeneste </h1>
          <form
            onSubmit={this.handleSubmit}
            noValidate
            className="form-main"
            id="form"
          >
            <div className="input-wrapper">
              <label htmlFor="title">Tittel: *</label>
              <input
                className="form-input"
                placeholder="..."
                type="text"
                name="title"
                noValidate
                onChange={this.handleChange}
              />
            </div>
            <div className="input-wrapper">
              <label htmlFor="logo">Logo:</label>
              <input
                className="form-input"
                placeholder="..."
                type="text"
                name="logo"
                noValidate
                onChange={this.handleChange}
              />
            </div>
            <div className="input-wrapper">
              <label htmlFor="extURL">Ekstern link til tjeneste:</label>
              <input
                className="form-input"
                placeholder="..."
                type="text"
                name="extURL"
                noValidate
                onChange={this.handleChange}
              />
            </div>
            <div className="input-wrapper">
              <label htmlFor="author">Forfatter: *</label>
              <input
                className="form-input"
                placeholder="..."
                type="text"
                name="author"
                noValidate
                onChange={this.handleChange}
              />
            </div>
          </form>
          <div className="form-description" form="form">
            <label htmlFor="content">Beskrivelse av tjenesten: *</label>
            <Quill getInformation={this.getQuill.bind(this)} />
          </div>
          <div className="form-select">
            <StatusMenu
              handleStatus={this.handleStatus}
              status={this.state.status}
              className="select status"
            />
            <div className="select category">
              <label>Kategori: *</label>
              <CreateableSelect
                form="form"
                isClearable
                isDisabled={isLoading}
                isLoading={isLoading}
                onChange={this.handleChangeCategory}
                onCreateOption={this.handleCreate}
                options={this.state.categoryList}
                value={category}
              />
            </div>
          </div>

          <div className="form-button create">
            <button type="submit" form="form" disabled={this.state.invalid}>
              Opprett tjeneste
            </button>
          </div>
          <div className="form-button cancel">
            <button onClick={this.handleCloseModal}>Avbryt</button>
          </div>
        </div>
      </ReactModal>
    );
  }
}

export default Form;
