import React from "react";
import axios from "axios";
import { ENDPOINT } from "../../utils/api";
import Topbar from "../Topbar/Topbar";
import TickerForm from "../Ticker/TickerForm";
import Footer from "../Footer/Footer.js";
import EditServiceForm from "./EditServiceForm.js";
import ntnuLogo from "../../media/images/logo_ntnu_u-slagord.png";
import "./Service.css";

import { withStyles } from "@material-ui/core/styles";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Paper
} from "@material-ui/core";

class Service extends React.Component {
  constructor(props) {
    super(props);
    this._isMounted = false;
    this.state = {
      service: [],
      tickers: [],
      redirect: false,
      modal: false,
      editRedirect: false,
      editModal: false,
      sid: this.props.match.params.id
    };
  }

  // Makes sure re-render takes effect when changing states
  componentDidMount() {
    this.fetchService();
    this.fetchServiceTickers();
    this._isMounted = true;
  }

  // Stops memory leak when user navigates to another component (stops setState on unmounted components)
  componentWillUnmount() {
    this._isMounted = false;
  }

  // Gets the service corresponding to the service_id
  fetchService = async () => {
    await axios
      .get(ENDPOINT + "api/service/" + this.props.match.params.id)
      .then(res => {
        this._isMounted && this.setState({ service: res.data });
      });
  };

  // Gets the tickers belonging to this service
  fetchServiceTickers = async () => {
    this._isMounted && this.setState({ tickers: [] });

    var pushServiceTickers = async (serviceId, serviceTitle) => {
      var newtickers = this.state.tickers;

      await axios.get(ENDPOINT + "api/ticker/all/" + serviceId).then(res2 =>
        res2.data.forEach(function(ticker) {
          var id = "";
          var title = "";
          var text = "";

          id = ticker.id;
          title = ticker.title;
          text = ticker.text;

          if (id) {
            newtickers.push({ serviceId, serviceTitle, id, title, text });
          }
        })
      );
      this._isMounted &&
        this.setState({
          tickers: newtickers
        });
    };

    await axios.get(ENDPOINT + "api/service/" + this.state.sid).then(res => {
      pushServiceTickers(res.data[0].id, res.data[0].title);
    });
  };

  onRedirect = () => {
    this.setState({ redirect: true });
  };

  // Makes sure the modal pops up when clicking the "Nytt problem" button
  onClickModal = () => {
    this.setState({ modal: true });
  };
  onCloseModal = () => {
    this.setState({ modal: false });
  };

  onEditRedirect = () => {
    this.setState({ editRedirect: true });
  };

  onEditClickModal = () => {
    this.setState({ editModal: true });
  };
  onEditCloseModal = () => {
    this.setState({
      editModal: false
    });
  };

  // Creates a table row with relevant data for the corresponding ticker
  renderTickers() {
    const { tickers } = this.state;

    return (
      <TableBody>
        {tickers.map(ticker => (
          <StyledTableRow key={ticker.id}>
            <StyledTableCell align="center">{ticker.id}</StyledTableCell>
            <StyledTableCell align="left">{ticker.title}</StyledTableCell>
            <StyledTableCell align="left">{ticker.text}</StyledTableCell>
          </StyledTableRow>
        ))}
      </TableBody>
    );
  }

  renderEditBtn = () => {
    if (this.props.admin) {
      return (
        <button className="service-btn edit" onClick={this.onEditClickModal}>
          <i className="far fa-edit"></i>
        </button>
      );
    }
  };

  checkImage = image => {
    if (!image) {
      return (
        <img
          id="icon24242"
          className="service-logo default"
          alt="icon"
          src={ntnuLogo}
        />
      );
    } else
      return (
        <img id="icon23411" className="service-logo" alt="icon" src={image} />
      );
  };

  renderService = () => {
    const fetchedService = this.state.service.map(service => (
      <div className="service-wrapper" key={service.id}>
        <div className="service-top">
          <div className="service-flex-top">
            {this.renderEditBtn()}
            <label className="service-title">{service.title}</label>
          </div>
          {this.checkImage(service.logo)}
        </div>

        <hr></hr>

        <div
          className="service-content"
          id="content"
          dangerouslySetInnerHTML={{ __html: service.content }}
        ></div>
        <div className="service-flex-bottom">
          <div className="service-url">
            <span className="service-links">Links</span>
            <br></br>
            <span>
              <a href={service.intURL} target="blank">
                NTNU Wiki
              </a>
            </span>
            <br></br>
            <span>
              <a href={service.extURL} target="blank">
                Hjemmeside
              </a>
            </span>
          </div>
          <div className="service-author wrapper">
            <br></br>
            <br></br>
            {service.author}
          </div>
        </div>
      </div>
    ));

    return fetchedService;
  };

  render() {
    return (
      <div className="container-service">
        <Topbar
          logout={true}
          home={true}
          newProject={false}
          about={true}
          clearSession={this.props.clearSession}
          admin={this.props.admin}
          adminBtn={true}
          hamburger={true}
        />
        <TickerForm
          modal={this.state.modal}
          sid={this.state.sid}
          user={this.props.user}
          token={this.props.token}
          close={this.onCloseModal}
        />
        <EditServiceForm
          editModal={this.state.editModal}
          serviceValues={this.state.service[0]}
          close={this.onEditCloseModal}
          token={this.props.token}
        />
        {this.renderService()}
        <div className="ticker-wrapper">
          <div className="ticker-title">Anmerkninger</div>
          <Paper className="ticker-paper">
            <Table className="ticker-table">
              <colgroup>
                <col style={{ width: "10%" }} />
                <col style={{ width: "20%" }} />
                <col style={{ width: "70%" }} />
              </colgroup>
              <TableHead>
                <TableRow>
                  <StyledTableCell align="center">ID</StyledTableCell>
                  <StyledTableCell align="left">Tittel</StyledTableCell>
                  <StyledTableCell align="left">Beskrivelse</StyledTableCell>
                </TableRow>
              </TableHead>
              {this.renderTickers()}
            </Table>
          </Paper>
        </div>
        <button className="ticker-btn-add" onClick={this.onClickModal}>
          Ny anmerkning
        </button>
        <Footer />
      </div>
    );
  }
}

// Helper functoin for styling table cells (ticker table)
const StyledTableCell = withStyles(theme => ({
  head: {
    backgroundColor: "#00509e",
    color: theme.palette.common.white,
    fontSize: "1em"
  },
  body: {
    fontSize: 14
  }
}))(TableCell);

// Helper function for styling table rows (ticker table)
const StyledTableRow = withStyles(theme => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.background.default
    }
  }
}))(TableRow);

export default Service;
