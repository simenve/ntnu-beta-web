import React from "react";
import { Redirect } from "react-router-dom";

import "./Landing.css";
import { authenticationService } from "../../authentication";
import Topbar from "../Topbar/Topbar";
import Footer from "../Footer/Footer";
import { ENDPOINT } from "../../utils/api";

class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: null,
      password: null,
      redirect: false,
      loggedIn: false
    };
  }

  componentDidMount() {
    if (this.props.user) {
      return <Redirect to="/" />;
    }
  }

  setRedirect = () => {
    this.setState({
      redirect: true
    });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/registration" />;
    }
    if (this.props.user) {
      return <Redirect to="/" />;
    }
  };

  handleSubmit = async e => {
    e.preventDefault();
    try {
      const response = await authenticationService.login(
        this.state.email,
        this.state.password
      );
      if (response.status === 200) {
        this.setState({ loggedIn: true });
        this.props.setUser();
      }
    } catch (err) {
      console.error(err);
    }
  };

  handleChange = e => {
    e.preventDefault();
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  render() {
    return (
      <div className="container-landing">
        <Topbar />
        {this.renderRedirect()}
        <div className="landing-wrapper">
          <h1 className="landing-title">Login</h1>
          <form
            onSubmit={this.handleSubmit}
            noValidate
            className="landing-form login"
            id="form"
            ref="form"
          >
            <div className="landing-form content">
              <input
                className="input"
                placeholder="E-post"
                autoComplete="off"
                name="email"
                type="text"
                onChange={this.handleChange}
              />
            </div>

            <div className="landing-form content">
              <input
                className="input"
                placeholder="Passord"
                name="password"
                type="password"
                onChange={this.handleChange}
              />
            </div>
            <div className="landing-btn login">
              <button type="submit" form="form">
                Logg inn
              </button>
            </div>
          </form>
          <div className="landing-link register">
            <label onClick={this.setRedirect}>Opprett en bruker</label>
          </div>
          <div className="landing-link feide">
            <a href={ENDPOINT + "api/auth/login"}>Logg inn med Feide</a>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default LoginPage;
