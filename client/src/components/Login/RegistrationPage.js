import React from "react";
import axios from "axios";
import { Redirect } from "react-router-dom";

import "./Landing.css";
import Topbar from "../Topbar/Topbar";
import Footer from "../Footer/Footer";
import { ENDPOINT } from "../../utils/api";

class RegistrationPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: null,
      password: null,
      redirect: false
    };
  }
  setRedirect = () => {
    this.setState({
      redirect: true
    });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/login" />;
    }
  };

  handleChange = e => {
    e.preventDefault();
    const { name, value } = e.target;
    this.setState({ [name]: value });
    console.log(name, value);
  };

  handleSubmit = async e => {
    e.preventDefault();
    await axios
      .post(ENDPOINT + "api/users", {
        email: this.state.email,
        password: this.state.password
      })
      .then(res => {
        console.log(res.data);
      });
    this.setRedirect();
  };

  render() {
    return (
      <div className="container-landing">
        <Topbar />
        {this.renderRedirect()}
        <div className="landing-wrapper">
          <div className="landing-link back">
            <label onClick={this.setRedirect}>Tilbake</label>
          </div>

          <h1 className="landing-title">Opprett ny bruker</h1>
          <form onSubmit={this.handleSubmit} className="landing-form register">
            <div className="landing-form content">
              <input
                className="input"
                placeholder="E-post"
                type="text"
                name="email"
                noValidate
                onChange={this.handleChange}
              />
            </div>

            <div className="landing-form content">
              <input
                className="input"
                placeholder="Passord"
                name="password"
                type="text"
                noValidate
                onChange={this.handleChange}
              />
            </div>
            <div className="landing-btn register">
              <button type="submit">Opprett bruker</button>
            </div>
          </form>
          <div>
            <p className="landing-info">
              For å kunne logge inn må du først bli godkjent av en
              administrator.
            </p>
          </div>
          <div />
        </div>
        <Footer />
      </div>
    );
  }
}

export default RegistrationPage;
