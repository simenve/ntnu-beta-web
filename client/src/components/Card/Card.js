import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './Card.css';

import ntnuLogo from '../../media/images/logo_ntnu_u-slagord.png';

class Card extends Component {
  
  status = status => {
    if (status === 'open') {
      return <p className='bottom-status-open'>I drift</p>;
    } else if (status === 'closed') {
      return <p className='bottom-status-closed'>Avsluttet</p>;
    } else if (status === 'testing') {
      return <p className='bottom-status-testing'>Under uttesting</p>;
    } else return null;
  };

  category = category => {
    return <p className='bottom-middle-category'>{category}</p>;
  };

  checkImage = image => {
    if (!image) {
      return <img id='icon' className='icon-ntnu' alt='icon' src={ntnuLogo} />;
    } else
      return (
        <img id='icon' className='icon' alt='icon' src={this.props.logo} />
      );
  };

  render() {
    return (
      <div className='card-wrapper' id={this.props.title}>
        <div className='top'>{this.checkImage(this.props.logo)}</div>
        <div className='middle'>
          <div className='middle-title'>{this.props.title}</div>
          <Link to={`/service/${this.props.id}`} className='link'>
            <h4 className='read-more'>Les mer</h4>
          </Link>
        </div>
        <div className='bottom'>
          <div className='bottom-left'>
            <div className='bottom-title'>Kategori</div>
            <div>{this.category(this.props.category)}</div>
          </div>
          <div className='bottom-right'>
            <div className='bottom-title'>Status</div>
            <div>{this.status(this.props.status)}</div>
          </div>
        </div>
      </div>
    );
  }
}

export default Card;
