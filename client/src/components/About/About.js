import React from 'react';
import './About.css';
import Topbar from '../Topbar/Topbar';
import Footer from '../Footer/Footer';

class About extends React.Component {
  render() {
    return (
      <div className="container-about">
        <Topbar
          admin={this.props.admin}
          adminBtn={true}
          newProject={false}
          logout={true}
          home={true}
          clearSession={this.props.clearSession}
          hamburger={true}
        />
        <div className='main-about'>
          <h1 className='title-about'>About headline</h1>
          <br></br>
          <p>
            In publishing and graphic design, Lorem ipsum is a placeholder text
            commonly used to demonstrate the visual form of a document without
            relying on meaningful content (also called greeking). Replacing the
            actual content with placeholder text allows designers to design the
            form of the content before the content itself has been produced. The
            lorem ipsum text is typically a scrambled section of De finibus
            bonorum et malorum, a 1st-century BC Latin text by Cicero, with
            words altered, added, and removed to make it nonsensical, improper
            Latin. A variation of the ordinary lorem ipsum text has been used in
            typesetting since the 1960s or earlier, when it was popularized by
            advertisements for Letraset transfer sheets. It was introduced to
            the information age in the mid-1980s by Aldus Corporation, which
            employed it in graphics and word-processing templates for its
            desktop publishing program PageMaker. Many popular word processors
            use this format as a placeholder. Some examples are Pages or
            Microsoft Word.
          </p>
        </div>
        <Footer />
      </div>
    );
  }
}

export default About;
