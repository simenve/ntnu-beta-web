import React, { Component } from "react";
import "./TickerForm.css";
import axios from "axios";
import { ENDPOINT } from "../../utils/api";
import ReactModal from "react-modal";

ReactModal.setAppElement("#root");
class TickerForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: null,
      text: null,
      mail: null,
      showModal: false,
      sid: this.props.sid,
      invalid: true
    };
    this.handleCloseModal = this.handleCloseModal.bind(this);
  }

  // Sets state based on props
  componentWillReceiveProps(props) {
    this.setState({ showModal: props.modal });
    this.setState({ invalid: true });
  }

  // Handles actions when form is submited. POST Request and redirection
  handleSubmit = async e => {
    e.preventDefault();
    await axios({
      url: ENDPOINT + "api/ticker/" + this.state.sid,
      method: "POST",
      headers: { "x-auth-token": this.props.token },
      data: {
        mail: this.state.mail,
        title: this.state.title,
        text: this.state.text
      }
    }).then(res => {
      window.location.reload();
    });

    this.handleCloseModal();
  };

  // Set state according to input
  handleChange = e => {
    e.preventDefault();
    const { name, value } = e.target;
    this.setState({ [name]: value });
    this.validateInput(name, value);
  };

  // Validates that the required fields have been filled in
  validateInput(name, value) {
    /*Maps the fields to assert their current contents.
    This is necessary because the setState function is asynchronous*/
    let title = name === "title" ? value : this.state.title;
    let mail = name === "mail" ? value : this.state.mail;
    let text = name === "text" ? value : this.state.text;
    if (!title || !mail || !text) {
      this.setState({ invalid: true });
      return;
    }
    this.setState({ invalid: false });
  }

  // Closes the form
  handleCloseModal() {
    this.setState({ showModal: false });
    this.props.close();
  }

  render() {
    return (
      <ReactModal
        isOpen={this.state.showModal}
        contentLabel="onRequestClose Example"
        onRequestClose={this.handleCloseModal}
        className="Modal"
        overlayClassName="Overlay"
      >
        <div className="ticker-form-wrapper">
          <h1 className="ticker-form-title">Opprett problem</h1>
          <form onSubmit={this.handleSubmit} noValidate id="form">
            <div className="input-wrapper">
              <label htmlFor="title">Tittel: *</label>
              <input
                className="form-input"
                placeholder="..."
                type="text"
                name="title"
                noValidate
                onChange={this.handleChange}
              />
            </div>
            <div className="input-wrapper">
              <label htmlFor="mail">Mail (kun synlig for Admin): *</label>
              <input
                className="form-input"
                placeholder="..."
                type="text"
                name="mail"
                noValidate
                onChange={this.handleChange}
              />
            </div>
          </form>
          <div className="input-wrapper" form="form">
            <label htmlFor="text">Beskrivelse av problemet: *</label>
            <textarea
              className="form-input textarea"
              rows="10"
              cols="30"
              placeholder="..."
              name="text"
              noValidate
              onChange={this.handleChange}
            />
          </div>
          <div className="ticker-btn-form create">
            <button type="submit" form="form" disabled={this.state.invalid}>
              Opprett problem
            </button>
          </div>
          <div className="ticker-btn-form cancel">
            <button onClick={this.handleCloseModal}>Avbryt</button>
          </div>
        </div>
      </ReactModal>
    );
  }
}

export default TickerForm;
