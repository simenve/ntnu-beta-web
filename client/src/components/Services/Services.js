import React from "react";
import axios from "axios";
import { ENDPOINT } from "../../utils/api";

import "./Services.css";
import "bootstrap/dist/css/bootstrap.min.css";

import Card from "../Card/Card.js";
import Form from "../ProjectForm/Form";
import Topbar from "../Topbar/Topbar";
import Footer from "../Footer/Footer.js";

class Services extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
      search: "",
      admin: true,
      modal: false,

      collecting: true,
      categoryListsByLabel: { Alle: [] },
      categories: [],
      choosenCat: "Alle"
    };
  }

  componentDidMount() {
    this.fetchServices();
  }

  fetchServices = async () => {
    let allServices = [];
    let categoryListsByLabel = {};
    categoryListsByLabel["Alle"] = allServices;

    await axios({
      url: ENDPOINT + "api/service",
      method: "GET"
      // headers: { 'x-auth-token': this.props.token }
    }).then(res => {
      res.data.forEach(function(service) {
        allServices.push(service);
        if (!categoryListsByLabel[service.category]) {
          categoryListsByLabel[service.category] = [];
        }
        categoryListsByLabel[service.category].push(service);
      });

      let categories = [];
      for (let key in categoryListsByLabel) {
        categories.push(createOption(key));
      }
      categories.sort((a, b) =>
        a.label > b.label ? 1 : b.label > a.label ? -1 : 0
      );

      this.setState({
        categoryListsByLabel: categoryListsByLabel,
        categories: categories,
        collecting: false
      });
    });
  };

  onRedirect = () => {
    this.setState({ redirect: true });
  };

  onClickModal = () => {
    this.setState({ modal: true });
  };
  onCloseModal = () => {
    this.setState({ modal: false });
  };

  handleSearch = e => {
    this.setState({ search: e.target.value.substr(0, 20) });
  };

  renderTopbar = () => {
    if (this.state.redirect === false) {
      return (
        <Topbar
          handleCategory={this.handleCategory}
          category={this.state.categories}
          onChange={this.handleSearch}
          onRedirect={this.onRedirect}
          onClickModal={this.onClickModal}
          searchBar={true}
          newProject={true}
          admin={this.props.admin}
          adminBtn={true}
          logout={true}
          clearSession={this.props.clearSession}
          token={this.props.token}
          about={true}
          hamburger={true}
        />
      );
    }
  };
  handleCategory = cat => {
    this.setState({ choosenCat: cat.value });
  };

  renderForm() {
    return (
      <Form
        modal={this.state.modal}
        user={this.props.user}
        token={this.props.token}
        close={this.onCloseModal}
      />
    );
  }

  render() {
    const { collecting, choosenCat, categoryListsByLabel } = this.state;

    var categoryFiltered = categoryListsByLabel[choosenCat];

    let filteredServices = categoryFiltered.filter(service => {
      return (
        service.title.toLowerCase().indexOf(this.state.search.toLowerCase()) !==
          -1 ||
        service.category
          .toLowerCase()
          .indexOf(this.state.search.toLowerCase()) !== -1
      );
    });

    const cards = filteredServices.map(service => (
      <Card
        key={service.id}
        id={service.id}
        title={service.title}
        logo={service.logo}
        status={service.status}
        category={service.category}
      />
    ));

    return (
      <div className="container-services">
        {this.renderTopbar()}
        {collecting ? "" : this.renderForm()}
        <div className="main-section">
          <div className="main-services">
            <div className="cards-wrapper">{cards}</div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

// Formats the value attribute of the input on category dropdown to lowercase
const createOption = label => ({
  label,
  value: label
});

export default Services;
