import React from 'react';
import { Route, Redirect } from 'react-router-dom';

export const AdminRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => {
        const currentUser = localStorage.getItem('currentUser');
        const feideUser = localStorage.getItem('userEmail');
        const user = currentUser || feideUser
        if (!user) {
            return <Redirect to="/login" />
        }
        if (currentUser) {
            const admin = JSON.parse(currentUser).data.admin;
            console.log(admin);
            if (admin === true) {
                return <Component {...props} {...rest} />
            }
        }
        return <Redirect to="/" />
    }} />
)