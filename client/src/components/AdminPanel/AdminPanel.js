import React from "react";
import axios from "axios";
import { Link } from "react-router-dom";

import "./AdminPanel.css";
import Topbar from "../Topbar/Topbar";
import Footer from "../Footer/Footer";
import { ENDPOINT } from "../../utils/api";

import { withStyles } from "@material-ui/core/styles";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Paper
} from "@material-ui/core";

import ScrollableTabsButtonPrevent from "./ScrollableTabsButtonPrevent";

class AdminPanel extends React.Component {
  constructor(props) {
    super(props);
    this._isMounted = false;
    this.state = {
      regulars: [],
      admins: [],
      requests: [],
      addUsers: [],
      selectedUser: null,
      rows: []
    };

    this.fetchUsers = this.fetchUsers.bind(this);
  }

  componentDidMount() {
    this.fetchUsers();
    this.fetchServiceTickers();
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  async fetchUsers() {
    await axios({
      url: ENDPOINT + "api/users",
      method: "GET",
      headers: { "x-auth-token": this.props.token }
    }).then(res => {
      const regulars = res.data
        .filter(function(user) {
          return (user.admin === 0) & (user.access === 1);
        })
        .map(user => user.email);
      const admins = res.data
        .filter(function(user) {
          return (user.admin === 1) & (user.access === 1);
        })
        .map(user => user.email);
      const requests = res.data
        .filter(function(user) {
          return user.access === 0;
        })
        .map(user => user.email);
      this.setState({ regulars: regulars, admins: admins, requests: requests });
    });
  }

  fetchUserUpdate = async (method, email, admin, access) => {
    await axios({
      method: method,
      url: `${ENDPOINT}api/users/${email}`,
      headers: { "x-auth-token": this.props.token },
      data: {
        admin: admin,
        access: access
      }
    }).then(res => {
      console.log(`${email} updated!`);
      if (admin === "True") {
        this.setState({
          regulars: this.state.regulars.filter(function(regular) {
            return regular !== email;
          })
        });
        this.setState({ admins: [...this.state.admins, email] });
      } else if (access === "True") {
        this.setState({
          admins: this.state.admins.filter(function(admin) {
            return admin !== email;
          })
        });
        this.setState({ regulars: [...this.state.regulars, email] });
      }
    });
  };

  fetchServiceTickers = async () => {
    this.setState({ rows: [] });

    var pushServiceTickers = async (serviceId, serviceTitle) => {
      var newRows = this.state.rows;

      await axios.get(ENDPOINT + "api/ticker/all/" + serviceId).then(res2 =>
        res2.data.forEach(function(ticker) {
          var id = "";
          var title = "";
          var text = "";
          var mail = "";

          id = ticker.id;
          title = ticker.title;
          text = ticker.text;
          mail = ticker.mail;

          if (id) {
            newRows.push({ serviceId, serviceTitle, id, title, text, mail });
          }
        })
      );
      this._isMounted &&
        this.setState({
          rows: newRows
        });
    };

    await axios.get(ENDPOINT + "api/service").then(res =>
      res.data.forEach(function(service) {
        pushServiceTickers(service.id, service.title);
      })
    );
  };

  handleSaveUser = user => {
    this.fetchUserUpdate("put", user, "False", "True");
    this.fetchUsers();
    this.setState({ selectedUser: null });
  };

  renderAdmins() {
    const { admins } = this.state;

    return (
      <TableBody>
        {admins.map(row => (
          <StyledTableRow key={row}>
            <StyledTableCell2 component="th" scope="row">
              {row}
            </StyledTableCell2>
            <StyledTableCell align="center">
              <button
                style={{
                  color: "red",
                  fontSize: "2em",
                  border: "none",
                  backgroundColor: "inherit",
                  cursor: "pointer"
                }}
                onClick={() => this.toRegular(row)}
              >
                &#215;
              </button>
            </StyledTableCell>
          </StyledTableRow>
        ))}
      </TableBody>
    );
  }

  renderRegulars() {
    const { regulars } = this.state;

    return (
      <TableBody>
        {regulars.map(row => (
          <StyledTableRow key={row}>
            <StyledTableCell2 component="th" scope="row">
              {row}
            </StyledTableCell2>
            <StyledTableCell align="center">
              <button
                style={{
                  color: "gold",
                  fontSize: "2em",
                  border: "none",
                  backgroundColor: "inherit",
                  cursor: "pointer"
                }}
                onClick={() => this.toAdmin(row)}
              >
                &#9733;
              </button>
            </StyledTableCell>
            <StyledTableCell align="center">
              <button
                style={{
                  color: "red",
                  fontSize: "2em",
                  border: "none",
                  backgroundColor: "inherit",
                  cursor: "pointer"
                }}
                onClick={() => this.handleDeleteUser(row)}
              >
                &#215;
              </button>
            </StyledTableCell>
          </StyledTableRow>
        ))}
      </TableBody>
    );
  }

  renderRequests() {
    const { requests } = this.state;

    return (
      <TableBody>
        {requests.map(row => (
          <StyledTableRow key={row}>
            <StyledTableCell2 component="th" scope="row">
              {row}
            </StyledTableCell2>
            <StyledTableCell align="center">
              <button
                style={{
                  color: "green",
                  fontSize: "2em",
                  border: "none",
                  backgroundColor: "inherit",
                  cursor: "pointer"
                }}
                onClick={() => {
                  this.setState({ selectedUser: row });
                  this.handleSaveUser(row);
                }}
              >
                &#10003;
              </button>
            </StyledTableCell>
            <StyledTableCell align="center">
              <button
                style={{
                  color: "red",
                  fontSize: "2em",
                  border: "none",
                  backgroundColor: "inherit",
                  cursor: "pointer"
                }}
                onClick={() => this.handleDeleteUser(row)}
              >
                &#215;
              </button>
            </StyledTableCell>
          </StyledTableRow>
        ))}
      </TableBody>
    );
  }

  selectedUser = e => {
    const user = e.target.innerHTML;
    this.setState({ selectedUser: user });
  };

  toRegular = user => {
    if (user === null) {
      return;
    }
    this.fetchUserUpdate("put", user, "False", "True");
    this.fetchUsers();
    this.setState({ selectedUser: null });
  };

  toAdmin = user => {
    if (user === null) {
      return;
    }
    this.fetchUserUpdate("put", user, "True", "True");
    this.fetchUsers();
    this.setState({ selectedUser: null });
  };

  handleDeleteUser = user => {
    if (user === null) {
      return;
    }
    this.fetchUserUpdate("delete", user);
    this.fetchUsers();
    this.setState({ selectedUser: null });
  };

  handleDeleteTicker = async id => {
    await axios({
      url: ENDPOINT + "api/ticker/" + id,
      method: "DELETE",
      headers: { "x-auth-token": this.props.token }
    });
  };

  handleTickerResolved = async id => {
    await axios({
      url: ENDPOINT + "api/ticker/" + id,
      method: "PUT",
      headers: { "x-auth-token": this.props.token }
    }).then(res =>
      this.setState({
        rows: this.state.rows.filter(function(row) {
          return row.id !== id;
        })
      })
    );
  };

  renderTickers() {
    const { rows } = this.state;

    return (
      <TableBody>
        {rows.map(row => (
          // key={row.id}
          <StyledTableRow key={row.id}>
            <StyledTableCell component="th" scope="row">
              <Link
                to={`/service/${row.serviceId}`}
                className="link"
                style={{ color: "black" }}
              >
                {row.serviceTitle}
              </Link>
            </StyledTableCell>
            <StyledTableCell align="center">
              <button
                style={{
                  color: "green",
                  fontSize: "2em",
                  border: "none",
                  backgroundColor: "inherit",
                  cursor: "pointer"
                }}
                onClick={() => this.handleTickerResolved(row.id)}
              >
                &#10003;
              </button>
            </StyledTableCell>
            <StyledTableCell align="center">{row.id}</StyledTableCell>
            <StyledTableCell align="left">{row.title}</StyledTableCell>
            <StyledTableCell align="left">{row.text}</StyledTableCell>
            <StyledTableCell align="left">{row.mail}</StyledTableCell>
          </StyledTableRow>
        ))}
      </TableBody>
    );
  }

  render() {
    return (
      <div className="container-adminpanel">
        <Topbar
          logout={true}
          home={true}
          clearSession={this.props.clearSession}
          admin={this.props.admin}
          adminBtn={false}
          about={true}
          hamburger={true}
        />
        <div className="dashboard" id="dashboard">
          <div className="content users">
            <h2>Brukere</h2>
            <ScrollableTabsButtonPrevent
              requests={this.renderRequests()}
              regulars={this.renderRegulars()}
              admins={this.renderAdmins()}
            />
          </div>
          <div className="content tickers">
            <h2>Tickers</h2>
            <Paper className="ticker-paper">
              <Table className={"ticker-table"}>
                <TableHead>
                  <TableRow>
                    <StyledTableCell align="left">Tjeneste</StyledTableCell>
                    <StyledTableCell align="left">Handling</StyledTableCell>
                    <StyledTableCell align="left">TickerID</StyledTableCell>
                    <StyledTableCell align="left">Tittel</StyledTableCell>
                    <StyledTableCell align="left">Beskrivelse</StyledTableCell>
                    <StyledTableCell align="left">Avsender</StyledTableCell>
                  </TableRow>
                </TableHead>
                {this.renderTickers()}
              </Table>
            </Paper>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

const StyledTableCell = withStyles(theme => ({
  head: {
    backgroundColor: "#00509e",
    color: theme.palette.common.white
  },
  body: {
    fontSize: 14
  }
}))(TableCell);

const StyledTableCell2 = withStyles(theme => ({
  head: {
    backgroundColor: "#00509e",
    color: theme.palette.common.white
  },
  body: {
    fontSize: 14,
    width: "100%"
  }
}))(TableCell);

const StyledTableRow = withStyles(theme => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.background.default
    }
  }
}))(TableRow);

export default AdminPanel;
