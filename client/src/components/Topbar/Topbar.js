import React from 'react';
import { Redirect } from 'react-router-dom';
import './Topbar.css';
import ntnuLogo from '../../media/images/ntnu-beta-logo.png';
import { Link } from 'react-router-dom';
import { authenticationService } from '../../authentication';
import Select from 'react-select';

class Topbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchBar: this.props.searchBar,
      logout: this.props.logout,
      newProject: this.props.newProject,
      admin: this.props.admin,
      home: this.props.home,
      about: this.props.about,
      loggedOut: false,
      toAdminPanel: false,
      toAbout: false,
      toHome: false,
      showSearch: false,
      responsive: false
    };
  }

  setRedirect = () => {
    this.setState({
      loggedOut: true
    });
  };

  renderRedirect = () => {
    if (this.state.loggedOut) {
      return <Redirect to='/login' />;
    } else if (this.state.toAdminPanel) {
      return <Redirect to='/admin' />;
    } else if (this.state.toAbout) {
      return <Redirect to='/about' />;
    } else if (this.state.toHome) {
      return <Redirect to='/' />;
    }
  };

  handleLogout = () => {
    this.setState({ loggedOut: true });
    authenticationService.logout();
    this.props.clearSession();
  };

  renderLogoutBtn() {
    if (this.state.logout) {
      return (
        <button className='button-default logout' onClick={this.handleLogout}>
          LOGG UT
        </button>
      );
    }
  }

  renderNewProjectBtn() {
    if (this.props.admin && this.props.newProject) {
      return (
        <button className='button-default' onClick={this.props.onClickModal}>
          NYTT PROSJEKT
        </button>
      );
    }
  }

  renderAdminBtn = () => {
    if (this.props.admin && this.props.adminBtn) {
      return (
        <button
          className='button-default'
          onClick={() => this.setState({ toAdminPanel: true })}>
          ADMIN
        </button>
      );
    }
  };

  renderHomeBtn = () => {
    if (this.state.home) {
      return (
        <button
          className='button-default'
          onClick={() => this.setState({ toHome: true })}>
          HJEM
        </button>
      );
    }
  };

  renderAboutBtn = () => {
    if (this.state.about) {
      return (
        <button
          className='button-default'
          onClick={() => this.setState({ toAbout: true })}>
          OM OSS
        </button>
      );
    }
  };
  renderSearchBar = () => {
    if (this.state.searchBar) {
      return (
        <div className='sb-wrapper'>
          <div className={this.state.showSearch ? 'search-bar active' : 'search-bar'}>
            <Select
              className='category-selector'
              options={this.props.category}
              placeholder='Kategori...'
              onChange={this.props.handleCategory}
            />
            <div className='input-field'>
              <input
                placeholder='Søk tjeneste...'
                onChange={this.props.onChange}
                className='input-search'
              />
            </div>
          </div>
          <i className='fas fa-search' onClick={() => this.setState({showSearch: !this.state.showSearch})}/>



        </div>
      );
    }
  };

  renderHamburger = () => {
    if (this.props.hamburger) {
      return (<div className="hamburger-menu" onClick={this.expandMenu}>
          <i className="fa fa-bars"></i>
      </div>);
    }
  }

  expandMenu = () => {
    const currentState = this.state.responsive;
    this.setState({ responsive: !currentState });
  }

  render() {
    return (
      <div className='topbar-container'>
        {this.renderRedirect()}
        <div className='topbar-logo'>
          <Link to='/'>
            <img
              className='logo'
              src={ntnuLogo}
              alt='Logo'
            />
          </Link>
        </div>
        <div className="topbar right">
        {this.renderSearchBar()}
        <div className={this.state.responsive ? 'button-wrapper responsive' : 'button-wrapper'}>
          {this.renderHamburger()}
          {this.renderAdminBtn()}
          {this.renderNewProjectBtn()}
          {this.renderHomeBtn()}
          {this.renderAboutBtn()}
          {this.renderLogoutBtn()}
        </div>
        </div>
      </div>
    );
  }
}

export default Topbar;
