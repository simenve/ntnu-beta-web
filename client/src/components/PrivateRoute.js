import React from 'react';
import { Route, Redirect } from 'react-router-dom';

export const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => {
        const currentUser = localStorage.getItem('currentUser') || localStorage.getItem('userEmail');
        if (!currentUser) {
            return <Redirect to="/login" />
        }
        return <Component {...props} {...rest} />
    }} />
)