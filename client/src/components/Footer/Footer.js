import React from 'react';
import './Footer.css';
import ntnuLogo from '../../media/images/logo_ntnu_u-slagord.png';

function Footer() {
    return (
        <div className="container-footer">

            <div className="footer-section logo">
                <img className="footer-icon" alt="icon" src={ntnuLogo} height="35px"/>
            </div>
            
            <div className="footer-section contact">
                <span className="footer-text">Ved spørsmål, kontakt </span>
                <a className="ntnu-beta" href="https://www.ntnu.no/drive/ntnu-beta" target="blank">NTNU Beta</a>
                <br/>
                <span className="epost">andreas.krokan@ntnu.no</span>
            </div>

            <div className="footer-section cookies">
                <span>Denne nettsiden bruker</span>
                <br/>
                <span>informasjonskapsler (cookies)</span>
            </div>
        </div>
    )
}

export default Footer;