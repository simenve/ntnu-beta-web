import React from "react";
import Services from "./components/Services/Services";
import Service from "./components/Service/Service";
import AdminPanel from "./components/AdminPanel/AdminPanel";
import LoginPage from "./components/Login/LoginPage";
import RegistrationPage from "./components/Login/RegistrationPage";
import About from "./components/About/About";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { PrivateRoute } from "./components/PrivateRoute";
import { AdminRoute } from "./components/AdminRoute";
import Axios from "axios";
import { ENDPOINT } from "./utils/api";

// The express server uses a self signed certificate for HTTPS
// The line below ignores the fact that the certificate is not trusted
// when doing API calls
// process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
      admin: false,
      token: null
    };
  }

  componentDidMount() {
    this.setUser();
    // this.checkFeide();
  }

  clearSession = () => {
    this.setState({ user: null, token: null, admin: false });
  };

  setUser = () => {
    const currentUser = localStorage.getItem("currentUser");
    const email = localStorage.getItem("userEmail");
    if (currentUser) {
      const admin = JSON.parse(currentUser).data.admin;
      const token = JSON.parse(currentUser).data.token;
      if (this.state.user !== email) {
        this.setState({ user: email, admin, token });
      }
    } else if (email) {
      if (this.state.user !== email) {
        this.setState({ user: email });
      }
    } else {
      this.checkFeide();
    }
  };

  checkFeide = async () => {
    if (!this.state.user) {
      await Axios({
        url: ENDPOINT + "api/logged-in",
        method: "GET"
      }).then(res => {
        localStorage.setItem("userEmail", res.data.email);
        this.setState({ user: res.data.email });
      });
    }
  };

  render() {
    return (
      <Router>
        <Switch>
          <React.Fragment>
            <PrivateRoute
              path="/"
              exact
              component={Services}
              user={this.state.user}
              token={this.state.token}
              clearSession={this.clearSession}
              admin={this.state.admin}
            />
            <Route
              path="/login"
              render={props => (
                <LoginPage
                  {...props}
                  setUser={this.setUser}
                  user={this.state.user}
                  checkFeide={this.checkFeide}
                />
              )}
            />
            <Route path="/registration" component={RegistrationPage} />
            <PrivateRoute
              path="/service/:id"
              component={Service}
              user={this.state.user}
              token={this.state.token}
              clearSession={this.clearSession}
              admin={this.state.admin}
            />
            <AdminRoute
              path="/admin"
              component={AdminPanel}
              admin={this.state.admin}
              user={this.state.user}
              token={this.state.token}
              clearSession={this.clearSession}
            />
            <PrivateRoute
              path="/about"
              component={About}
              user={this.state.user}
              token={this.state.token}
              clearSession={this.clearSession}
              admin={this.state.admin}
            />
          </React.Fragment>
        </Switch>
      </Router>
    );
  }
}

export default App;
