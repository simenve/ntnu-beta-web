import axios from "axios";
import React from "react";
import { Redirect } from "react-router-dom";
import { ENDPOINT } from "./utils/api";

axios.defaults.withCredentials = true;

const login = async (email, password) => {
  localStorage.removeItem("currentUser");

  let response;

  await axios({
    method: "POST",
    headers: { "Content-Type": "application/json" },
    url: `${ENDPOINT}api/login`,
    data: {
      email: email,
      password: password
    }
  })
    .then(res => {
      localStorage.setItem("currentUser", JSON.stringify(res));
      localStorage.setItem("userEmail", JSON.stringify(email));
      response = res;
    })
    .catch(function(error) {
      console.error(error);
      return error;
    });

  return response;
};

async function logout() {
  localStorage.removeItem("currentUser");
  localStorage.removeItem("userEmail");
  await axios.get(ENDPOINT + "api/auth/logout").then(res => {
    return <Redirect to="/login" />;
  });
}

export const authenticationService = {
  login,
  logout,
  user: localStorage.getItem("currentUser")
};
